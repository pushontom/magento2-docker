PushON Ltd Docker Env for Magento 2 Development for Ubuntu
==============================
 
Prerequisites:
 docker 
 docker-compose
 ubuntu
 
Clone the repo to your home folder, then run: 
docker-compose up --build -d

Once this is booted up, create the following path: ~/Code/magento2/public_html

Copy ~/magento2-docker/installs/magento2.sh to ~/Code/magento2/ 

Set the permissions on magento2.sh to executable: chmod +x magento2.sh
Now run the script: . magento2.sh

This pull down Magneto 2 for you and composer install it. 

Once this is done, you should be able to visit magento2.dev in your browser (assuming your hosts file points to it!)






